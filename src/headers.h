#pragma once

// Args
#include <stdarg.h>
// General
#include <iostream>
#include <fstream>
#include <sstream>
#include <istream>
#include <ostream>
#include <ios>
//
#include <string>
#include <vector>
#include <set>
#include <regex>
#include <filesystem>
#include <cstdint>
#include <algorithm>

// Pre-libs defs, since libs tend to be buggy as ...
#ifndef	_CRT_SECURE_NO_DEPRECATE
	/*
	#pragma warning(push)
	#pragma warning(disable:4127)
	#pragma warning(disable:4244)
	#pragma warning(disable:4311)
	#pragma warning(disable:4312)
	#pragma warning(disable:4512)
	#pragma warning(disable:4571)
	#pragma warning(disable:4640)
	#pragma warning(disable:4706)
	#pragma warning(disable:4710)
	#pragma warning(disable:4800)
	#pragma warning(disable:4804)
	#pragma warning(disable:4820)
	#pragma warning(disable:4996)
	*/
#define _CRT_SECURE_NO_DEPRECATE 1
#define _CRT_SECURE_NO_WARNINGS 1
#define _CRT_NONSTDC_NO_DEPRECATE 1
#endif

#ifndef _OPENVDB_DISABLE_SPAM
	#pragma warning(disable:4146)	// unary minus operator applied to unsigned type, result still unsigned
	#pragma warning(disable:4251)	// 'openvdb::v6_1::math::ScaleMap::mScaleValues': class 'openvdb::v6_1::math::Vec3<double>' needs to have dll-interface to be used by clients of class 'openvdb::v6_1::math::ScaleMap'
	#pragma warning(disable:4267)	// 'argument': conversion from 'size_t' to 'long', possible loss of data
	#pragma warning(disable:4244)	// 'argument': conversion from 'int64_t' to 'long', possible loss of data
	#pragma warning(disable:4275)	// non dll-interface class 'std::exception' used as base for dll-interface class 'openvdb::v6_1::Exception'
	#pragma warning(disable:4996)	// 'argument': conversion from 'int64_t' to 'long', possible loss of data
#define _OPENVDB_DISABLE_SPAM
#endif


// Libs
#include "omp.h"
#include "re2/re2.h"
#include "fmt/format.h"
//
#include <openvdb/openvdb.h>
#include "openvdb/tools/dense.h"
//
#include <OpenImageIO/imageio.h>

//#define INCLUDE_WINDOWS_H
#undef MOUSE_MOVED
#include "curses.h"


#ifndef COUT_SET
	#define COUT_SET {cout.setf(std::ios::fixed); cout.precision(8);}
	#define OSTR_SET {ostr.setf(std::ios::fixed); ostr.precision(8);}
#endif

#ifndef M_PI
	#define M_PI 3.14159265358979323
#endif


// Cast native types
typedef uint8_t		U8;
typedef uint16_t	U16;
typedef uint32_t	U32;
typedef uint64_t	U64;

typedef int8_t		I8;
typedef int16_t		I16;
typedef int32_t		I32;
typedef int64_t		I64;

// Custom types
typedef U8					Byte;
typedef std::vector<Byte>	Bytes;
typedef	I32					Int;
typedef	I64					Lint;
typedef	float				Float;
typedef	double				Double;
typedef	Float				Real;
#define REAL_MIN			FLOAT_MIN;
#define REAL_MAX			FLOAT_MAX;
typedef	std::string			String;
typedef	std::vector<String>	Strings;

