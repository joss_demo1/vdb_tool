#pragma once
#include "headers.h"

typedef std::vector<unsigned char> byteArray;

class FileIO
{
public:
	FileIO(std::string&);
	~FileIO();

	void clear();
	
	void set(std::string&);
	void setData(byteArray&);
	void setDataS(std::string&);
	std::string get(void) const;
	std::string getBak(void) const;
	byteArray getData(void) const;
	std::string getDataS(void) const;

	void read(void);
	int	write(void);
	_int64 length(void);
	bool backup(void);
	

private:
	std::string	_fname;
	std::string	_fbak;
	int			_hasData;
	byteArray	_buffer;
	byteArray	_readFile(std::string&);
	bool		_writeFile(std::string&, byteArray&);


};
