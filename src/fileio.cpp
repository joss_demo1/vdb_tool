#include "fileio.h"
using namespace std;


FileIO::FileIO(std::string &fname)
{
	clear();
	set(fname);
}


FileIO::~FileIO()
{
}


void FileIO::clear()
{
	_buffer.clear();
	_hasData = false;
	_fname = "";
	_fbak = "";
}

void FileIO::set(std::string& fname)
{
	_fname = fname;
	_hasData = false;
}

void FileIO::setData(byteArray &indata)
{
	_buffer = indata;
	_hasData = true;
}

void FileIO::setDataS(std::string& indata)
{
	_buffer.clear();
	_buffer = byteArray { indata.begin(), indata.end() };
	_hasData = true;
}

std::string FileIO::get(void) const
{
	return _fname;
}

std::string FileIO::getBak(void) const
{
	return _fbak;
}

byteArray FileIO::getData(void) const
{
	return _buffer;
}

std::string FileIO::getDataS(void) const
{
	std::string strbuf{ _buffer.begin(), _buffer.end() };
	return strbuf;
}


void FileIO::read(void)
{
	if (!_fname.size())
		return;
	_buffer = _readFile(_fname);
	_hasData = true;
}

int FileIO::write(void)
{
	if(_hasData)
		return _writeFile(_fname, _buffer);
	return 0;
}

_int64 FileIO::length(void)
{
	return static_cast<_int64>(_buffer.size());
}


bool FileIO::backup()
{
	namespace fs = std::filesystem;
	const fs::path srcPath = _fname;
	const fs::path dstPath = _fname+".bak";
	fs::copy_options cpOpts = fs::copy_options::overwrite_existing;
	std::error_code err;

	auto result = fs::copy_file(srcPath, dstPath, cpOpts, err);
	if (result) _fbak = dstPath.generic_string();
	else _fbak = std::string("");
	return result;
}



// private:
byteArray FileIO::_readFile(std::string& fname)
{
	ifstream F;
	F.open(fname, ios::binary | ios::ate);
	if (!F) { return byteArray(); }
	_int64 flen = static_cast<_int64>(F.tellg());
	if (flen <= 0) { F.close(); return byteArray(); }
	F.seekg(0, ios_base::beg);

	byteArray buf(flen);
	F.read(reinterpret_cast<char*>(buf.data()), flen);
	return buf;
}



bool FileIO::_writeFile(std::string& fname, byteArray& buffer)
{
	ofstream F;
	F.open(fname, ios::out | ios::binary);
	if (!F) return false;
	F.write(reinterpret_cast<char*>(buffer.data()), buffer.size());
	return true;
}


