## VDB Tool:
============

### Big picture:
  - take list of files
  - sort by name
  - load slice from each file into memory buffer
  - build `openvdb.tools.dense` grid
  - dump the result into VDB



## Random links:

- [ ] [scipy](http://scipy-lectures.org/advanced/interfacing_with_c/interfacing_with_c.html)
- [ ] [VDB Dense](https://www.openvdb.org/documentation/doxygen/Dense_8h.html#details)

